var urls = [];

var video;
var vidsdiv;
var controlsPlay;
var controlsTime;
var timeSlider;
var controlsCurrentTime;

var maxDuration = 0;
var currentTime = 0;
var vids = [];
var loadedVids = 0;
var vidIndex = null;

var vidswitch = null;
var switchVidTimeout = null;
var timeo = Date.now();
var timedifitsloaded = 0;
var timebeforeendtocheck = 0.6;

var removeElemAfterLoaded = null;


function stopVidToFakeLoad(e) {
  e.target.pause();
  e.target.removeEventListener("timeupdate", stopVidToFakeLoad);
  e.target.currentTime = 0;
  e.target.muted = false;

  maxDuration += e.target.duration;
  loadedVids++;
  console.log(e.target.id + " loaded, duration: " + e.target.duration);

  // loaded last vid
  if (loadedVids == vids.length) {
    var elem = document.querySelector("#slice-player");
    elem.style.display = "block";

    elem = document.querySelector(removeElemAfterLoaded);
    if (elem != null) {
      elem.remove();
    }


    controlsTime.innerHTML = parseInt(maxDuration).toString().toHHMMSS();
    timeSlider.min = 0;
    timeSlider.max = maxDuration;
    timeSlider.value = 0;
    controlsPlay.disabled = false;
  }
}

function pauseoldVid(e) {
  vids[vidIndex -1].pause();
  vids[vidIndex -1].style.display = "none";
  vids[vidIndex].style.display = "block";
  e.target.removeEventListener("timeupdate", pauseoldVid);
}

function runNextVidWithTimeout(e) {
  var tt = Date.now() - timeo;
  tt = (timedifitsloaded * 1000) - tt;

  if (tt < 0) {
    timebeforeendtocheck += (tt *-1) + 0.1;
  }

  e.target.pause();
  e.target.removeEventListener("timeupdate", runNextVidWithTimeout);
  e.target.currentTime = 0;

  console.log(e.target.id + " play in " + tt);
  switchVidTimeout = setTimeout(function() {
    e.target.addEventListener("timeupdate", pauseoldVid);
    e.target.currentTime = 0;
    e.target.play();

    if (vidIndex >= vids.length -1) {
      // do not start interval
    } else {
      vidswitch = setInterval(checkForVidEnd, 10);
    }
  }, tt);
}

function checkForVidEnd() {
  if (vids[vidIndex].currentTime >= vids[vidIndex].duration - timebeforeendtocheck) {
    clearInterval(vidswitch);

    if (vidIndex +1 < vids.length) {
      timedifitsloaded = vids[vidIndex].duration - vids[vidIndex].currentTime;
      timeo = Date.now();

      vidIndex += 1;

      console.log("add event");
      vids[vidIndex].addEventListener("timeupdate", runNextVidWithTimeout);
      vids[vidIndex].play();
    } else {
      // do not start another interval
      console.log("end intervaller");
    }
  }
}

function loadSlicePlayer(elem, urls, rmElem = null) {
  removeElemAfterLoaded = rmElem;

  elem.innerHTML += `
  <div id="slice-player" style="display: none;">
    <div id="vids">
    </div>
    <div id="controls" style="display: table; width: 100%;">
      <div style="display: table-row;">
        <div style="display: table-cell;"><input type="button" id="play" value="play" /></div>
        <div style="display: table-cell;"><code id="current-time">0:00</code></div>
        <div style="display: table-cell; width: 100%;"><input type="range" min="0" max="100" value="0" style="width: 100%;" id="timeSlider"></div>
        <div style="display: table-cell;"><code id="time">0:00</code></div>
      </div>
    </div>
  </div>
  `;

  setTimeout(function() {
    // load vars
    video = document.querySelector('video');
    vidsdiv = document.querySelector('#vids');
    controlsPlay = document.querySelector('#controls #play');
    controlsTime = document.querySelector('#controls #time');
    timeSlider = document.querySelector('#controls #timeSlider');
    controlsCurrentTime = document.querySelector('#controls #current-time');


    // disable play button until we load all vids
    controlsPlay.disabled = true;

    // load all vids to hidden
    for (var i = 0; i < urls.length; i++) {
      vidsdiv.innerHTML += `
      <video src="`+urls[i]+`" id="vid`+i+`" style="display: none;" disabled></video>
      `;
    }


    // add control events

    controlsPlay.onclick = function() {
      if (vids[vidIndex].playing) {
        clearInterval(vidswitch);
        clearTimeout(switchVidTimeout);

        vids[vidIndex].pause();
        controlsPlay.value = "play";
      } else {
        vids[vidIndex].play();
        controlsPlay.value = "pause";

        if (vidIndex >= vids.length -1) {
          // do not start interval
        } else {
          vidswitch = setInterval(checkForVidEnd, 10);
        }
      }
    };

    timeSlider.oninput = function() {
      // get time on chunk
      var totalTimeAlready = 0;
      for (var i = 0; i < vids.length; i++) {
        totalTimeAlready += vids[i].duration;

        if (totalTimeAlready > this.value) {
          // got right chunk
          console.log("selected time on slider: " + this.value);

          clearTimeout(switchVidTimeout);

          // set current vid
          vidIndex = i;

          // clear all chunks
          for (var j = 0; j < vids.length; j++) {
            vids[j].style.display = "none";
            vids[j].pause();
            //vids[j].currentTime = 0;
          }

          // sho right chunk
          vids[i].style.display = "block";
          // set chunk time
          vids[i].currentTime = this.value - (totalTimeAlready - vids[i].duration);
          // play chunk
          vids[i].play();

          break;
        }
      }
    };


    // load all videos,
    // problem on vivaldi browser when user has to interact vid first,
    // and that causes pretty bad video switching
    for (var i = 0; i < urls.length; i++) {
      setTimeout(function(j) {
        var vid = document.querySelector('#vid'+j);
        vid.addEventListener("timeupdate", stopVidToFakeLoad);
        vids.push(vid);
        vid.muted = true;
        vid.play();

        if (j == urls.length -1) {
          // and run main code now

          vidIndex = 0;

          var intervo = setInterval(function() {
            currentTime = 0;
            for (var i = 0; i < vidIndex; i++) {
              currentTime += vids[i].duration;
            }
            currentTime += vids[vidIndex].currentTime;

            controlsCurrentTime.innerHTML = parseInt(currentTime).toString().toHHMMSS();
            timeSlider.value = currentTime;
          }, 100);

          if (vidIndex >= vids.length -1) {
            // do not start interval
          } else {
            vidswitch = setInterval(checkForVidEnd, 10);
          }

          vids[vidIndex].style.display = "block";
        }
      }, 100 * i, i);
    }

  }, 1000);

}
