// recursive function that loads thumbnail one by one
function loadThumbnail(i, episodes) {
  var dataUrl = getURLParameter("data");

  var str = `
  <div class="single-right-grids">
    <div class="col-md-4 single-right-grid-left">
      <a href="view.html?ep=`+i+`&data=`+dataUrl+`" id="thumbnail-`+i+`"></a>
    </div>
    <div class="col-md-8 single-right-grid-right">
      <a href="view.html?ep=`+i+`&data=`+dataUrl+`" class="title">`+episodes[i]["name"]+`</a>
      <p id="thumbnail-loading-`+i+`" class="views">loading</p>
      <p class="author">`+(i == parseInt(getURLParameter("ep")) ? "selected" : "")+`</p>
    </div>
    <div class="clearfix"> </div>
  </div>
  `;

  document.querySelectorAll(".single-grid-right")[0].innerHTML += str;
/* web torrent way not really good tho :P
  var thumbClient = new WebTorrent();
  thumbClient.add(episodes[i]["wide-thumb"], function (torrent) {
    var thumbnail = torrent.files.find(function (file) {
      return file.name.startsWith('wide-thumb');
    });

    if (document.getElementById("thumbnail-"+i)) {
      thumbnail.appendTo('#hidden-image-container');

      document.getElementById("thumbnail-"+i).appendChild(document.querySelector("#hidden-image-container").firstChild);
      $("#thumbnail-"+i+" img").on("load", function() {
        document.getElementById("thumbnail-loading-"+i).remove();
        delete thumbClient;
      }).on("error", function() {
        $("thumbnail-loading-"+i+"").innerHTML = "error loading";
      });
    }
  });
*/
  if (document.getElementById("thumbnail-"+i)) {
    var thumbnailo = "<img src='"+dataUrl+'/'+episodes[i]["wide-thumb"]+"' alt='"+episodes[i]["name"]+"-thumbnail' />";

    document.getElementById("thumbnail-"+i).innerHTML = thumbnailo;
    $("#thumbnail-"+i+" img").on("load", function() {
      document.getElementById("thumbnail-loading-"+i).remove();
      delete thumbClient;
    }).on("error", function() {
      $("thumbnail-loading-"+i+"").innerHTML = "error loading";
    });
  }

  // load next thumbnail
  if (i +1 < episodes.length) {
    loadThumbnail(i +1, episodes);
  }
}

function loadWebtorrentVideo() {
  // make loading thingy
  document.querySelector(".video-grid[name='webtorrent']").innerHTML += "<div class='wait-video' id='wait-video-webtorrent'><img src='img/wait.png' /></div>";

  if (window.RTCPeerConnection || window.mozRTCPeerConnection) {
    // noice we got rtc bla bla
  } else {
    page.cmd("wrapperNotification", ["info", "Your browser does not support WebRTC", 5000]);
    return 0;
  }

  page.wakeSeeder();
  var client = new WebTorrent();
  var gotDownloading = false;

  // HTML elements
  var $progressBar = document.querySelector('#progressBar');
  var $numPeers = document.querySelector('#numPeers');
  var $downloaded = document.querySelector('#downloaded');
  var $total = document.querySelector('#total');
  var $remaining = document.querySelector('#remaining');
  var $uploadSpeed = document.querySelector('#uploadSpeed');
  var $downloadSpeed = document.querySelector('#downloadSpeed');
  var episode = parseInt(getURLParameter("ep"));

  client.on('error', function (err) {
    console.error('ERROR: ' + err.message);
    page.showNotification("main video webrtc client failed, trying again");
  });

  // set litte timeout to check after 20 seconds did we got downloading
  var gotDownloadingTimout = setTimeout(function() {
    if (gotDownloading == false) {
      page.wakeSeeder();
      page.cmd("wrapperNotification", ["info", "Waking up server for that episode, takes usually 10 secconds", 5000]);
    }
  }, 30000);

  // get magent link and display video
  page.getMagnetLink(function (magnet) {
    client.add(magnet, function (torrent) {
      // Torrents can contain many files. Let's use the .webm file
      var file = torrent.files.find(function (file) {
        return file.name.startsWith('vid');
      });

      // remove wait image
      document.getElementById("wait-video-webtorrent").remove();

      // Display the file by adding it to the DOM.
      // Supports video, audio, image files, and more!
      file.appendTo(".video-grid[name='webtorrent']");

      // Trigger statistics refresh
      torrent.on('done', onDone);
      var nullProgressCounter = 0;

      var sitvo = setInterval(onProgress, 500);
      onProgress();

      // Statistics
      function onProgress () {
        // Peers
        $numPeers.innerHTML = torrent.numPeers + (torrent.numPeers === 1 ? ' peer' : ' peers');

        // check if were downloading or not
        // let it tick for 5 seconds
        if (torrent.numPeers > 0 && torrent.progress == 0 && nullProgressCounter < 10) {
          nullProgressCounter++;

          if (nullProgressCounter >= 10) {
            clearInterval(sitvo);
            clearTimeout(gotDownloadingTimout);
            client.remove(magnet, function() {});
            document.getElementsByTagName("video")[0].remove();
            page.cmd("wrapperNotification", ["info", "Reloading WebTorrent, cause old didnt seem to download", 5000]);
            loadWebtorrentVideo();
          }
        } else if (torrent.progress > 0) {
          gotDownloading = true;
        }

        // Progress
        var percent = Math.round(torrent.progress * 100 * 100) / 100;
        $progressBar.style.width = percent + '%';
        $downloaded.innerHTML = prettyBytes(torrent.downloaded);
        $total.innerHTML = prettyBytes(torrent.length);

        // Remaining time
        var remaining
        if (torrent.done) {
          remaining = 'Done.'
        } else {
          remaining = moment.duration(torrent.timeRemaining / 1000, 'seconds').humanize()
          remaining = remaining[0].toUpperCase() + remaining.substring(1) + ' remaining.'
        }
        $remaining.innerHTML = remaining

        // Speed rates
        $downloadSpeed.innerHTML = prettyBytes(torrent.downloadSpeed) + '/s'
        $uploadSpeed.innerHTML = prettyBytes(torrent.uploadSpeed) + '/s'
      }

      function onDone () {
        onProgress()
      }
    });
  });
}

function loadHostedslicesVideo() {
  var elem = document.querySelector(".video-grid[name='hostedslices']");

  // make loading thingy
  elem.innerHTML += "<div class='wait-video' id='wait-video-hostedslices'><img src='img/wait.png' /></div>";

  page.getSlices(function(slices) {
    loadSlicePlayer(elem, slices, "#wait-video-hostedslices");
  });
}

// Human readable bytes util
function prettyBytes(num) {
  var exponent, unit, neg = num < 0, units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  if (neg) num = -num;
  if (num < 1) return (neg ? '-' : '') + num + ' B';
  exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1);
  num = Number((num / Math.pow(1000, exponent)).toFixed(2));
  unit = units[exponent];
  return (neg ? '-' : '') + num + ' ' + unit
}

function changeSource(elem) {
  // deactivate all sources
  var elems = document.querySelectorAll(".source-buttons ul li a");
  for (var i = 0; i < elems.length; i++) {
    elems[i].className = "top";
  }

  // hide all vid grids
  var elems = document.querySelectorAll(".video-grid");
  for (var i = 0; i < elems.length; i++) {
    elems[i].style.display = "none";
  }

  // make select one bright
  elem.className = "top active";

  // display vid grid view based source
  document.querySelector(".video-grid[name='"+elem.id+"']").style.display = "block";
}

function changeSourceRes(elem) {
  // deactivate resolutions
  var elems = document.querySelectorAll("#vidResolutions ul li a");
  for (var i = 0; i < elems.length; i++) {
    elems[i].className = "top";
  }

  // remove all videos on grids
  var elems = document.querySelectorAll(".video-grid video");
  for (var i = 0; i < elems.length; i++) {
    elems[i].remove();
  }

  var playVideoOptionalBigfile = document.getElementById("play-video-optionalbigfile");

  // if we have hided main play button that bring out vid then show it again and add click listener
  if (playVideoOptionalBigfile.style.display === "none") {
    playVideoOptionalBigfile.style.display = "block";
    playVideoOptionalBigfile.addEventListener("click", playOptionalbigfile);
  }


  // make select one bright
  elem.className = "top active";
}

function playOptionalbigfile(e) {
  console.log("view.js - playOptionalbigfile("+e+")");

  // remove play video image
  var playVideoOptionalBigfile = document.getElementById("play-video-optionalbigfile");
  playVideoOptionalBigfile.removeEventListener("click", playOptionalbigfile);
  playVideoOptionalBigfile.style.display = "none";

  page.getCurrentEpisode(function(episode) {
    var dataUrl = getURLParameter("data");

    var elems = document.querySelectorAll("#vidResolutions ul li a");
    if (elems && elems.length > 0) {
      for (var i = 0; i < elems.length ; i++) {
        if (elems[i].className === "top active") {
          document.querySelector(".video-grid[name='optionalbigfile']").innerHTML += `
          <video poster="`+dataUrl+"/"+episode["wide-thumb"]+`" src="`+dataUrl+"/"+episode["optionals"][parseInt(elems[i].id)]["path"]+`" controls></video>`;
          break;
        }
      }
    } else {
      document.querySelector(".video-grid[name='optionalbigfile']").innerHTML += `
      <video poster="`+dataUrl+"/"+episode["wide-thumb"]+`" src="`+dataUrl+"/"+episode["optional"]+`" controls></video>`;
    }
  });

}

function playHostedslices(e) {
  console.log("view.js - playHostedslices("+e+")");

  // remove play video image
  var playVideoHostedslices = document.getElementById("play-video-hostedslices");
  playVideoHostedslices.removeEventListener("click", playHostedslices);
  playVideoHostedslices.remove();

  loadHostedslicesVideo();
}

function playWebtorrent(e) {
  console.log("view.js - playWebtorrent("+e+")");

  // remove play video image
  var playVideoWebtorrent = document.getElementById("play-video-webtorrent");
  playVideoWebtorrent.removeEventListener("click", playWebtorrent);
  playVideoWebtorrent.remove();

  loadWebtorrentVideo();
}

setTimeout(function() {
  // get episodes
  page.getEpisodes(function (episodes) {
    loadThumbnail(0, episodes);
  });

  // load resources
  page.getSources(function(sources) {
    console.log("view.js - got sources: " + sources);

    // optionals also have quality options
    if (sources.indexOf("optionals") > -1) {
      document.querySelector(".source-buttons ul").innerHTML += `
      <li><a href='#main' onclick='changeSource(this)' id="optionalbigfile" class='top'>Optional bigfile</a></li>
      `;
      document.querySelector("#video-players").innerHTML += `
      <div class='video-grid' name='optionalbigfile'>
        <div class='source-buttons' id="vidResolutions"><ul>
        </ul></div>
        
        <p>! Note that u r going to download that episode and seed it !</p><b/>
        <div class='play-video' id='play-video-optionalbigfile'><img src='img/play.png' /></div>
      </div>
      `;

      // wait little so innerhtml is set
      setTimeout(function() {
        page.getCurrentEpisode(function(episode) {
          for (var i = 0; i < episode["optionals"].length ; i++) {
            document.querySelector("#vidResolutions ul").innerHTML += `
            <li><a href='#main' onclick='changeSourceRes(this)' id="`+i+`" class='top`+(i === 0 ? " active" : "")+`'>`+episode["optionals"][i]["res"]+`</a></li>`;
          }
        });

        // add click listeners
        document.getElementById("play-video-optionalbigfile").addEventListener("click", playOptionalbigfile);
      }, 100);
    } else if (sources.indexOf("optional") > -1) {
      document.querySelector(".source-buttons ul").innerHTML += `
      <li><a href='#main' onclick='changeSource(this)' id="optionalbigfile" class='top'>Optional bigfile</a></li>
      `;
      document.querySelector("#video-players").innerHTML += `
      <div class='video-grid' name='optionalbigfile'>
        <p>! Note that u r going to download that episode and seed it !</p><b/>
        <div class='play-video' id='play-video-optionalbigfile'><img src='img/play.png' /></div>
      </div>
      `;

      // wait little so innerhtml is set
      setTimeout(function() {
        // add click listeners
        document.getElementById("play-video-optionalbigfile").addEventListener("click", playOptionalbigfile);
      }, 100);
    }

    if (sources.indexOf("slices") > -1) {
      document.querySelector(".source-buttons ul").innerHTML += `
      <li><a href='#main' onclick='changeSource(this)' id="hostedslices" class='top'>Hosted slices</a></li>
      `;
      document.querySelector("#video-players").innerHTML += `
      <div class='video-grid' name='hostedslices'>
        <p>! Note that u might notice playing next slice, every browser is difference !</p><b/>
        <div class='play-video' id='play-video-hostedslices'><img src='img/play.png' /></div>
      </div>
      `;

      setTimeout(function() {
        // add click listeners
        document.getElementById("play-video-hostedslices").addEventListener("click", playHostedslices);
      }, 100);
    }

    if (sources.indexOf("magnet") > -1) {
      document.querySelector(".source-buttons ul").innerHTML += `
      <li><a href='#main'  onclick='changeSource(this)' id="webtorrent" class='top'>WebTorrent</a></li>
      `;
      document.querySelector("#video-players").innerHTML += `
      <div class='video-grid' name='webtorrent'>
        <!-- Statistics -->
        <div id='status'>
          <div id='stStatus'>
            <p>! Note that connecting to seeder may take up to 30 seconds !</p><b/>
            <span class='show-leech'>Downloading </span>
            <span class='show-seed'>Seeding </span>
            <span class='show-leech'> from </span>
            <span class='show-seed'> to </span>
            <code id='numPeers'>0 peers</code>
            &#x2198;<code id='downloadSpeed'>0 b/s</code>
            / &#x2197;<code id='uploadSpeed'>0 b/s</code>
          </div>
          <div>
            <div id='progressBar'></div>
            <code id='downloaded'></code>
            of <code id='total'></code>
            — <span id='remaining'></span>
          </div>
        </div>

        <div class='play-video' id='play-video-webtorrent'><img src='img/play.png' /></div>
        <!-- simple loading image goes here first  -->
        <!-- then video gets entered here -->
      </div>
      `;

      setTimeout(function() {
        // add click listeners
        document.getElementById("play-video-webtorrent").addEventListener("click", playWebtorrent);
      }, 100);
    }

    // wut vid source we have selected
    var vidSource = getURLParameter("source");

    if (document.querySelector("#optionalbigfile") != undefined && (vidSource == null || vidSource == "optionalbigfile")) {
      changeSource(document.querySelector("#optionalbigfile"));
    } else if (document.querySelector("#hostedslices") != undefined && (vidSource == null || vidSource == "hostedslices")) {
      changeSource(document.querySelector("#hostedslices"));
    } else if (document.querySelector("#webtorrent") != undefined && (vidSource == null || vidSource == "webtorrent")) {
      changeSource(document.querySelector("#webtorrent"));
    }
  });
}, 500);
