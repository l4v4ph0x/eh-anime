Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
};

NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&|#]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

String.prototype.toHHMMSS = function () {
  var sec_num = parseInt(this, 10); // don't forget the second param
  var hours   = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);

  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}
  //return hours+':'+minutes+':'+seconds;
  // no need hours right now
  return minutes+':'+seconds;
};

Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
  get: function(){
    return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
  }
});

var animeFiles = [];
var wakeServer = "http://35.204.229.11:4567/wake";

class ZeroChat extends ZeroFrame {
  onOpenWebsocket () {
    // sucessfully connected
  }

  onRequest (cmd, message) {
    if (cmd == "setSiteInfo") {
      var selectUserElem = document.getElementById("select_user")

      if (message.params.cert_user_id) {
        if (selectUserElem) {
          selectUserElem.innerHTML = message.params.cert_user_id
        }
      } else {
        if (selectUserElem) {
          document.getElementById("select_user").innerHTML = "Select user"
        }
      }

      // Save site info data to allow access it later
      this.siteInfo = message.params
    }
  }

  selectUser () {
    this.cmd("certSelect", {accepted_domains: ["zeroid.bit"]});
    return false;
  }

  loadFiles (loadUpdatedFiles = false) {
    var self = this;

    self.cmd("fileQuery", ["anime/*/data.json", ""], (files) => {
      // check if files count have changed to call got new files event or function
      if (loadUpdatedFiles && animeFiles.length > 0 && animeFiles.length < files.length) {
        var newFiles = files.filter(function (el) {
          var i = 0;
          for (; i < animeFiles.length; i++) {
            if (el["name"] === animeFiles[i]["name"]) {
              break;
            }
          }
          return i === animeFiles.length;
        });

        animeFiles = files;
        self.gotNewFiles(newFiles);
      } else {
        animeFiles = files;
      }
    });

    // if loadUpdatedFiles is set then load thi function again to see ew anime files
    if (loadUpdatedFiles) {
      setTimeout(function() {
        // infinity call every 10 seconds to check new animes
        self.loadFiles(loadUpdatedFiles);
      }, 10000);
    }
  }

  loadHtml (htmlfile = "index", elemQuery = "body", execJs = false, loadFiles = false, loadUpdatedFiles = false) {
    var self = this;

    if (loadFiles) {
      this.loadFiles(loadUpdatedFiles);
    }

    var elem = document.querySelectorAll(elemQuery)[0];
    if (elem) {
      elem.innerHTML += "";
    }

    this.cmd("fileGet", ["html/"+htmlfile+".html", ""], (data) => {
      try {
        var elem = document.querySelectorAll(elemQuery)[0];
        elem.innerHTML += data;

        if (execJs) {
          var jsElems = elem.querySelectorAll("script[exec='true']");
          if (jsElems != undefined) {
            for (var i = 0; i < jsElems.length; i++) {
              eval(jsElems[i].innerHTML);
            }
          }
        }
      } catch (err) {
        console.error('ERROR: ' + err.message);
        setTimeout(function() {
          self.loadHtml(htmlfile, elem, execJs, loadFiles);
        }, 1000);
      }
    });
  }

  writeComments (posts) {
    var sorted = posts.sort(function(a, b) {
      if (a["date"] < b["date"]) return 1;
      if (a["date"]  > b["date"]) return -1;
      return 0;
    });

    var elem = document.querySelectorAll(".media-grids")[0];
    if (elem) {
      // clear comments
      elem.innerHTML = "";

      for (var i = 0; i < sorted.length; i++) {
        var msg = sorted[i]["body"];
        var lt = /</g,
            gt = />/g,
            ap = /'/g,
            ic = /"/g;
        msg = msg.toString().replace(lt, "&lt;").replace(gt, "&gt;").replace(ap, "&#39;").replace(ic, "&#34;");

        elem.innerHTML += `
        <div class='media'>
          <h5>`+sorted[i]["name"]+`</h5>
          <div class='media-left'>
            <img src='img/u12.png' />
          </div>
          <div class='media-body'>
            <p>`+msg+`</p>
          </div>
        </div>
        `;
      }
    }
  }

  loadComments (limit = 20, filtero = "") {
    var dataUrl = getURLParameter("data");
    var animeName = dataUrl.split('/')[1];

    // automatically select user if its been selected recently
    this.cmd("siteInfo", {}, (siteInfo) => {
      this.siteInfo = siteInfo;
      if (this.siteInfo && this.siteInfo.cert_user_id) {
        document.getElementById("select_user").innerHTML = this.siteInfo.cert_user_id
      }
    });

    this.cmd("fileQuery", ["data/users/*/data.json", ""], (files) => {
      var posts = [];

      for (var i = 0; i < files.length; i++) {
        var data = files[i]

        // set max 20 comments to load at first as set on argument
        for (var j = 0; j < data["messages"].length && i < limit; j++) {
          if (data["messages"][j]["anime"] == animeName) {
            posts.push(data["messages"][j])
          }
        }
      }

      if (filtero != "") {
        var filtered = posts.filter(function (el) {
          return el["auth_address"] == filtero;
        });

        posts = filtered;
      }

      this.writeComments(posts);
    });
  }

  loadAllComments () {
    // not really all gets loaded for memory overflow reason
    this.loadComments(1000);
  }

  loadMyComments () {
    if (this.siteInfo && this.siteInfo.auth_address) {
      // not really all gets loaded for memory overflow reason
      this.loadComments(1000, this.siteInfo.auth_address);
    } else {
      this.cmd("wrapperNotification", ["info", "Please, select your account."]);
    }
  }

  addComment () {
    var dataUrl = getURLParameter("data");
    var animeName = dataUrl.split('/')[1];

    if (!this.siteInfo || !this.siteInfo.cert_user_id) {
      // No account selected, display error
      this.cmd("wrapperNotification", ["info", "Please, select your account.", 5000]);
      return false;
    }

    if (document.getElementById("enter-message").value.length > 0) {
      // This is our data file path
      var data_inner_path = "data/users/" + this.siteInfo.auth_address + "/data.json"
      var content_inner_path = "data/users/" + this.siteInfo.auth_address + "/content.json"
      var cert_user_id = this.siteInfo.cert_user_id
      var auth_address = this.siteInfo.auth_address

      // Load our current messages
      this.cmd("fileGet", {"inner_path": data_inner_path, "required": false}, (data) => {
        if (data)  {
          // Parse current data file
          data = JSON.parse(data);
        } else {
          // Not exists yet, use default data
          data = { "messages": [] }
        }

        // Add the new message to data
        data.messages.push({
          "anime": animeName,
          "body": document.getElementById("enter-message").value,
          "date": Date.now(),
          "name": cert_user_id.split('@')[0],
          "auth_address": auth_address
        });

        // Encode data array to utf8 json text
        var json_raw = unescape(encodeURIComponent(JSON.stringify(data, undefined, '\t')))

        // Write file to disk
        this.cmd("fileWrite", [data_inner_path, btoa(json_raw)], (res) => {
          if (res == "ok") {
            // Reset the message input
            document.getElementById("enter-message").value = '';

            // Sign the changed file in our user's directory
            this.cmd("siteSign", {"inner_path": content_inner_path}, (res) => {
              // Publish to other users
              this.cmd("sitePublish", {"inner_path": content_inner_path, "sign": false})
            });

            this.loadComments();
          } else {
            this.cmd("wrapperNotification", ["error", "File write error: #{res}"])
          }
        });
      });
    } else {
      this.cmd("wrapperNotification", ["info", "Enter some text to post comment.", 5000]);
    }
  }

  wakeSeeder () {
    var dataUrl = getURLParameter("data");
    var animeName = dataUrl.split('/')[1]
    var episode = parseInt(getURLParameter("ep"));

    try {
      // wake server to start sreaming
      var xmlHttp = new XMLHttpRequest();
      // false for synchronous request
      xmlHttp.open("GET", wakeServer + "?anime=" + animeName + "&ep=" + (episode +1), true);
      xmlHttp.send(null);
      console.log(xmlHttp.responseText);
    } catch (err) {
      console.error('ERROR: ' + err.message);
    }
  }

  getMagnetLink (callback) {
    var dataUrl = getURLParameter("data");
    var episode = parseInt(getURLParameter("ep"));

    this.cmd("fileQuery", [dataUrl + "/data.json", ""], (files) => {
      try {
        callback(files[0]["episodes"][episode]["magnet"]);
      } catch (err) {
        console.error('ERROR: ' + err.message);
      }
    });
  }

  getEpisodes (callback) {
    var dataUrl = getURLParameter("data");

    this.cmd("fileQuery", [dataUrl + "/data.json", ""], (files) => {
      callback(files[0]["episodes"]);
    });
  }

  getCurrentEpisode (callback) {
    var dataUrl = getURLParameter("data");
    var episode = parseInt(getURLParameter("ep"));

    this.cmd("fileQuery", [dataUrl + "/data.json", ""], (files) => {
      callback(files[0]["episodes"][episode]);
    });
  }

  getSources (callback) {
    var dataUrl = getURLParameter("data");
    var episode = parseInt(getURLParameter("ep"));

    this.cmd("fileQuery", [dataUrl + "/data.json", ""], (files) => {
      var ret = [];

      if (files[0]["episodes"][episode]["optionals"] != undefined) {
        ret.push("optionals");
      }
      if (files[0]["episodes"][episode]["optional"] != undefined) {
        ret.push("optional");
      }
      if (files[0]["episodes"][episode]["slices"] != undefined) {
        ret.push("slices");
      }
      if (files[0]["episodes"][episode]["magnet"] != undefined) {
        ret.push("magnet");
      }

      callback(ret);
    });
  }

  getSlices (callback) {
    var dataUrl = getURLParameter("data");
    var episode = parseInt(getURLParameter("ep"));

    this.cmd("fileQuery", [dataUrl + "/data.json", ""], (files) => {
      callback(files[0]["episodes"][episode]["slices"]);
    });
  }

  loadPageCounter (pagesCount = 0) {
    var str = `<div class="heading-right">`;
    var pageCounter = getURLParameter("page") == null ? 1 : parseInt(getURLParameter("page")) +1;

    for (var i = 1; i <= pagesCount; i++) {
      str += `<span style="padding: 4px;"><a href="newest.html?page=`+(i -1)+`" class="play-icon popup-with-zoom-anim">`+(pageCounter == i ? "<b>"+i+"</b>" : "<i>"+i+"</i>")+`</a></span>`;
    }
    str += `</div></div><div class="clearfix"> </div><br /><br />`;

    var high = `<div class="col-md-2 col-md-offset-6">` + str;
    var low = `<div class="col-md-2 col-md-offset-5">` + str;

    document.getElementById("high-page-counter").innerHTML += high;
    document.getElementById("low-page-counter").innerHTML += low;
  }

  drawFiles (files = [], start = 0, count = 0, insertPosition = -1) {
    var elem = document.querySelector("#insert-newest");
    var str = "";

    if (insertPosition != -1) {
      for (var i = 0; i < elem.children.length && i < insertPosition; i++) {
        str += elem.children[i].outerHTML;
      }
    }

    for (var i = start; i < files.length && i < start + count; i++) {
      var jo = files[i]

      var viewUrl = "anime/"+jo["inner_path"];

      str += `
      <div class="col-md-3 resent-grid recommended-grid movie-video-grid w3-animate-opacity">
        <div class="resent-grid-img recommended-grid-img">
          <a href="view.html?ep=0&data=`+viewUrl+`"><img src="anime/`+jo["inner_path"]+'/'+jo["thumb"]+`" alt="" /></a>
          <div class="time small-time show-time movie-time">
            <p>`+jo["duration"]+`</p>
          </div>
          <div class="clck movie-clock">
            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
          </div>
        </div>
        <div class="resent-grid-info recommended-grid-info recommended-grid-movie-info">
          <h5><a href="view.html?ep=0&data=`+viewUrl+`" class="title">`+jo["name"]+`</a></h5>
          <ul>
            <li><p class="author author-info"><a class="author">`+jo["flag"]+`</a></p></li>
            <li class="right-list"><p class="views views-info">`+jo["episodes"].length+" / "+jo["episodesCount"]+`</p></li>
          </ul>
        </div>
      </div>
      `;
    }

    if (insertPosition != -1) {
      for (var i = insertPosition; i < elem.children.length; i++) {
        str += elem.children[i].outerHTML;
      }
    }

    elem.innerHTML = str;
  }

  loadNewest (maxOnPage = 16, pageCounter = false) {
    var files = animeFiles;

    if (pageCounter) {
      this.loadPageCounter(parseInt(files.length / maxOnPage) +1);
    }
    var start = getURLParameter("page") == null ? 0 : parseInt(getURLParameter("page")) * maxOnPage;

    var dateSorted = files.sort(function(a, b) {
      if (a["date"] < b["date"]) return 1;
      if (a["date"]  > b["date"]) return -1;
      return 0;
    });

    this.drawFiles(dateSorted, start, maxOnPage);
  }

  loadRecentlyUpdated () {
    var self = this;

    self.cmd("fileGet", ["recentlyUpdated.json", ""], (data) => {
      try {
        if (data) {
          var jdata = JSON.parse(data);
          var elem = document.querySelector("#recentlyUpdatedCaroucel");

          if (elem) {
            var divos = [];

            jdata["recentyl updated"].forEach(function(item) {
              var imgPath = item["path"].includes("/ep") ? item["path"] + "/wide-thumb.jpg" : item["path"] + "/thumb.jpg";
              var viewUrl = item["path"].includes("/ep") ? "view.html?ep=" + (parseInt(item["path"].split("/ep")[1]) -1) + "&data=" + item["path"].split("/ep")[0] : "view.html?ep=0&data=" + item["path"];

              divos.push(`
              <div class="col-md-3 resent-grid recommended-grid slider-first w3-animate-opacity">
                <div class="resent-grid-img recommended-grid-img">
                  <a href="`+viewUrl+`"><img src="`+imgPath+`" alt=""></a>
                </div>
                <div class="resent-grid-info recommended-grid-info">
                  <h5><a href="`+viewUrl+`" class="title">`+item["title"]+`</a></h5>
                </div>
              </div>`);
            });

            var str = `
             <ol class="carousel-indicators">`;
            for (var i = 0; i < divos.length; i += 4) {
              str += `
              <li data-target="#recentlyUpdatedCaroucel" data-slide-to="0" class="active"></li>`;
            }
            str += `
            </ol>`;

            // split items into gourp, every group has 4 items
            for (var i = 0; i < divos.length; i += 4) {
              str += `
              <div class="carousel-inner">`;

              for (var j = 0; j < 4 && j < divos.length; j++) {
                str += divos[i + j];
              }
              str += `
              </div>`;
            }

            str += `</ul>`;
            elem.innerHTML = str + elem.innerHTML;
          } else {
            // element doesnt exists
          }
        } else {
          // we dont have `recentlyUpdated.json` downloaded yet
          // then reload again
          setTimeout(function() {
            self.loadRecentlyUpdated();
          }, 1000);
        }


      } catch (err) {
        console.error('ERROR: ' + err.message);
        setTimeout(function() {
          self.loadRecentlyUpdated();
        }, 1000);
      }
    });
  }

  search (txt = "", maxOnPage = 16, pageCounter = false) {
    var files = animeFiles;

    // clear olds
    try {
      document.querySelector("#insert-newest").innerHTML = "";

      document.getElementById("high-page-counter").innerHTML = "";
      document.getElementById("low-page-counter").innerHTML = "";
    } catch (err) {
      console.error('ERROR: ' + err.message);
    }

    if (pageCounter) {
      this.loadPageCounter(parseInt(files.length / maxOnPage) +1);
    }
    var start = getURLParameter("page") == null ? 0 : parseInt(getURLParameter("page")) * maxOnPage;

    var filtered = files.filter(function (el) {
      return el["name"].toLowerCase().includes(txt.toLowerCase());
    });

    var dateSorted = filtered.sort(function(a, b) {
      if (a["date"] < b["date"]) return 1;
      if (a["date"]  > b["date"]) return -1;
      return 0;
    });

    this.drawFiles(dateSorted, start, maxOnPage);
  }

  gotNewFiles (newFiles) {
    console.log("lilFrame.js - gotNewFiles("+newFiles+")");

    var files = animeFiles;

    // sort all files including new ones by date
    var dateSorted = files.sort(function(a, b) {
      if (a["date"] < b["date"]) return 1;
      if (a["date"]  > b["date"]) return -1;
      return 0;
    });

    // loop to find place where to insert new file with sorted by date
    for (var i = 0; i < dateSorted.length; i++) {
      for (var j = 0; j < newFiles.length; j++) {
        if (dateSorted[i]["name"] == newFiles[j]["name"]) {
          this.drawFiles(dateSorted, i, 1, i);
        }
      }
    }
  }

  showNotification (msg = "", type ="info") {
    this.cmd("wrapperNotification", [type, msg]);
  }
}

page = new ZeroChat();
